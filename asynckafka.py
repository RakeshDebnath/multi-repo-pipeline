from kafka import KafkaConsumer, KafkaProducer

def consume():
    consumer = KafkaConsumer('testnaming_download', bootstrap_servers=['localhost:9092'],
                             auto_offset_reset='earliest', consumer_timeout_ms=2000)
    offset = 0
    for message in consumer:
        print(type(message.offset), message.offset, message.value)
        offset = int(message.offset)
        print(offset)


def produce():
    producer = KafkaProducer(bootstrap_servers=['localhost:9092'])
    producer.send('first', b'Supratim halder')
    print('sent')


if __name__ == '__main__':
    produce()
    consume()
