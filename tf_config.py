from enumerations import ModuleEnum, AttributeEnum

kafka_group_size_term_filter__corpus_filter = 1
kafka_group_size_doc_filter__corpus_filter = 1
kafka_group_size_term_filter__ontology = 1
models = [(ModuleEnum.WORDSPACE_UNSUPERVISED, AttributeEnum.STATUS)]
