import logging
# import logging_util
import tf_config as config
from user_input import UserInput
from kafka_utils import KafkaService
from zookeeper_utils import Zookeeper, CallBack as ZookeeperCallback
from enumerations import ModuleEnum, AttributeEnum, PhaseEnum, LearningType, StatusEnum
from classes.kafka_classes import ProducerReponseDownload

logger = logging.getLogger('controller_driver')
models = {}


def build_deactivate():
    for model in config.models:
        module = model[0]
        attribute = model[1]
        model_status = Zookeeper.read(module, attribute)
        if not model_status:
            logger.debug(f'{module.value} is not completed yet. Not de-activating new_build')
            break
        Zookeeper.set_build_complete()


def send_to_doc_reader():
    logger.info(f'Notifying document reader about files.')
    Zookeeper.write(ModuleEnum.CONTROLLER, AttributeEnum.SEED_DOCUMENT_PROCESSED, value=True)
    logger.debug(f'CONTROLLER COMPLETED | SENT {Zookeeper.read(ModuleEnum.CONTROLLER, AttributeEnum.SEED_DOCUMENT_COUNT)} SEED FILES')


def send_to_structure_extractor():
    # todo: Send saved golden doc urls to the structure extractor. Also set a flag at the end,
    # todo: Add a node under controller to notify the SE that golden dox are given. Done.
    logger.debug(f'Sending to structure extractor')
    golden_documents = UserInput.get_golden_documents()
    logger.debug(f'Given golden documents: {golden_documents}')
    Zookeeper.write(ModuleEnum.STRUCTURE_EXTRACTOR, AttributeEnum.GOLDEN_DOCUMENTS, value=True)


def send_for_download(terms=False, ctxs=False):
    logger.debug(f'Sending terms for download')
    # golden_term_location = Zookeeper.read(ModuleEnum.CONFIG, AttributeEnum.GOLDEN_TERM)
    # ctx_term_location = Zookeeper.read(ModuleEnum.CONFIG, AttributeEnum.CONTEXT_TERMS)
    topic = Zookeeper.read(ModuleEnum.DOWNLOADER, AttributeEnum.INPUT_Q)

    if terms:
        # write to output Q for downloader with the terms
        KafkaService.send(topic, None,
                          {'data': True,
                           'actualData': '',
                           'type': 'url_dir',
                           'context': [],
                           'download_type': 'terms'},
                          ProducerReponseDownload())
        logger.info(f'Terms data sent for download at {topic}')

    '''if ctxs:
        # write to output Q for downloader with the ctxs
        KafkaService.send(topic, None,
                          {'data': ctx_term_location,
                           'actualData': ctx_term_location,
                           'type': 'url_dir',
                           'context': [],
                           'download_type': 'ctx_terms'},
                          ProducerReponseDownload())
        logger.info(f'Ctx data sent for download at {topic}')'''

    Zookeeper.write(ModuleEnum.CONTROLLER, AttributeEnum.TERM_PROCESSED, value=True)  # todo: Add node in zk


def notify_ontology_curator():
    Zookeeper.write(ModuleEnum.CONTROLLER, AttributeEnum.BASE_ONTOLOGY, value=True)


def reset_nodes():
    logger.info(f'Resetting MSG_COUNT and PROCESSED Nodes for CONTROLLER')
    Zookeeper.write(ModuleEnum.CONTROLLER, AttributeEnum.DOCUMENT_COUNT, 0)
    Zookeeper.write(ModuleEnum.CONTROLLER, AttributeEnum.SEED_DOCUMENT_COUNT, 0)
    Zookeeper.write(ModuleEnum.CONTROLLER, AttributeEnum.GOLDEN_DOCUMENT_COUNT, 0)
    Zookeeper.write(ModuleEnum.CONTROLLER, AttributeEnum.TERM_PROCESSED, value=False)
    Zookeeper.write(ModuleEnum.CONTROLLER, AttributeEnum.DOCUMENT_PROCESSED, value=False)
    Zookeeper.write(ModuleEnum.CONTROLLER, AttributeEnum.STATUS, value=StatusEnum.AWAITING)