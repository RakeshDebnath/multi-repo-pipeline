import sys
import time
import tf_config
import logging
# import logging_util
from storage_utils import Storage
from kafka_utils import KafkaService
from zookeeper_utils import Zookeeper, CallBack as ZookeeperCallback
from enumerations import ModuleEnum, AttributeEnum, PhaseEnum, LearningType, StatusEnum

from classes.zk_classes import UserReviewTermsResponse, CoreNlpResponse, UserReviewDocsResponse,\
    IterationChangeResponse, PhaseChangeResponse, OCuration, DistributeData, WodrspaceResponse
from classes.kafka_classes import ReDownloadKafkaResponse

logger = logging.getLogger('controller_driver')


def new_build_active():
    logger.debug('Do all build level ZK initializations and watch registrations here!')
    # There should be a generic signal from starter, stating that the data has been saved.
    Zookeeper.watch(ModuleEnum.STARTER, AttributeEnum.STATUS, DistributeData())

    # Watch on the rdf parser completion.
    Zookeeper.watch(ModuleEnum.ONTOLOGY_CURATOR, AttributeEnum.STATUS, OCuration())
    Zookeeper.watch(ModuleEnum.TERM_USER_REVIEW, AttributeEnum.STATUS, UserReviewTermsResponse())
    Zookeeper.watch(ModuleEnum.CORE_NLP, AttributeEnum.STATUS, CoreNlpResponse())
    Zookeeper.watch(ModuleEnum.DOC_USER_REVIEW, AttributeEnum.STATUS, UserReviewDocsResponse())
    Zookeeper.watch(ModuleEnum.CONFIG, AttributeEnum.CORPUS_ITERATION, IterationChangeResponse())
    Zookeeper.watch(ModuleEnum.CONFIG, AttributeEnum.LEARNING_ITERATION, IterationChangeResponse())
    Zookeeper.watch(ModuleEnum.CONFIG, AttributeEnum.PHASE, PhaseChangeResponse())
    Zookeeper.watch(ModuleEnum.WORDSPACE_UNSUPERVISED, AttributeEnum.STATUS, WodrspaceResponse())
    return True


if __name__ == '__main__':
    for _ in range(tf_config.kafka_group_size_term_filter__corpus_filter):
        KafkaService.receive(Zookeeper.read(ModuleEnum.TERM_FILTER, AttributeEnum.INPUT_Q_CORPUS_F),
                             'TermFilterGroup_1', ReDownloadKafkaResponse())
    while True:
        time.sleep(10)
